const peopleSalary = [
    {"id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":"40000"},
    {"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},
    {"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},
    {"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":"9000000"}
    ]

let thName = "";
let tableHeader = "";
let tableRow = "";


for(let i=0; i<peopleSalary.length; i++) {
    let counter = 0;
    thName = "";
    let tdValue = "";
    for (let key in  peopleSalary[i]) {
        counter++;
        let value;

        if (counter != 4) {
            thName += "<th>" + key + "</th>";
        }
        
    }
    
}
tableHeader = "<tr>" + thName + "</tr>";
$('#myTable').append($(tableHeader));



for(let i=0; i<peopleSalary.length; i++) {
    let counter = 0;
    let tdValue = "";
    let olValue = "";
    let newSalary = [];
    const keyChecker1 = "company";
    const keyChecker2 = "salary";
    for (let key in peopleSalary[i]) {
        value = peopleSalary[i][key];

        if (key != keyChecker1) {
            if (key == keyChecker2) {
                let liValue = "";
                let keepValue = 0;
                let setValue = 0;
                for(let i=0; i<3; i++) {
                    setValue = parseInt(value) + parseInt(value*0.1);
                    keepValue = keepValue + setValue;
                    newSalary.push(keepValue);
                    if (i == 0) {
                        liValue += "<li>" + value + "</li>";
                    }
                    else
                        liValue += "<li>" + keepValue + "</li>";
                    }
                olValue += "<ol>" + liValue + "</ol>";
                tdValue += "<td>" + olValue + "</td>";
    
            }
            else {
                tdValue += "<td>" + value + "</td>";


            }

        }   
        
    }
    tableRow = "<tr>" + tdValue + "</tr>";
    //alert(genTd);
    $('#myTable').append($(tableRow));
}
