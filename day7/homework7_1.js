let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const result = arr.filter(arr => {
    return ( arr % 2 === 0);
});

const map1 = result.map(x => x * 1000);
console.log(map1);