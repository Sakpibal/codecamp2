debugger;

const path = 'homework2_1.json'

fetch(path).then(response => {
    return response.json();
})
    .then(peopleSalary => {
        employeeFlex(peopleSalary);
        showTableResult(peopleSalary);
    })
    .catch(error => {
        console.error('Error:', error);
    });


function employeeFlex(peopleSalary) {
    // console.log(peopleSalary);

    const peopleLowSalary = peopleSalary.filter(peopleSalary => {
        return peopleSalary.salary < 1000000;
    });
    // console.log(peopleLowSalary);


    const peopleIncreaseSalary = peopleLowSalary.map(peopleLowSalary => ({
        firstname: peopleLowSalary.firstname, lastname: peopleLowSalary.lastname,
        company: peopleLowSalary.company, salary: parseInt(peopleLowSalary.salary) * 2
    }));
    console.log(peopleIncreaseSalary);

    const peopleNotIncreaseSalary = peopleSalary.filter(peopleSalary => {
        return peopleSalary.salary >= 100000;
    });
    console.log(peopleNotIncreaseSalary);

    const result1 = peopleIncreaseSalary.reduce((sum, cur) => sum + parseInt(cur.salary), 0);
    console.log("result1 value is : " + result1);

    const result2 = peopleNotIncreaseSalary.reduce((sum, cur) => sum + parseInt(cur.salary), 0);
    console.log("result2 value is : " + result2);
    // arr.reduce((a, b) => ({x: a.x + b.x}));
}


function lowSalary(peopleSalary) {
    const peopleLowSalary = peopleSalary.filter(peopleSalary => {
        return peopleSalary.salary < 1000000;
    });
    // console.log(peopleLowSalary);


    const peopleIncreaseSalary = peopleLowSalary.map(peopleLowSalary => ({
        firstname: peopleLowSalary.firstname, lastname: peopleLowSalary.lastname,
        company: peopleLowSalary.company, salary: parseInt(peopleLowSalary.salary) * 2
    }));
    console.log(peopleIncreaseSalary);
    const result1 = peopleIncreaseSalary.reduce((sum, cur) => sum + parseInt(cur.salary), 0);
    console.log("result1 value is : " + result1);

    return result1;
}

function hightSalary(peopleSalary) {
    const peopleNotIncreaseSalary = peopleSalary.filter(peopleSalary => {
        return peopleSalary.salary >= 100000;
    });


    const result2 = peopleNotIncreaseSalary.reduce((sum, cur) => sum + parseInt(cur.salary), 0);
    return result2;
    console.log("result2 value is : " + result2);
    return result2;
}

function showPeopleLowSalary(peopleSalary) {
    const peopleLowSalary = peopleSalary.filter(peopleSalary => {
        return peopleSalary.salary < 1000000;
    });
    // console.log(peopleLowSalary);


    const peopleIncreaseSalary = peopleLowSalary.map(peopleLowSalary => ({
        id: peopleLowSalary.id, firstname: peopleLowSalary.firstname, lastname: peopleLowSalary.lastname,
        company: peopleLowSalary.company, salary: parseInt(peopleLowSalary.salary) * 2
    }));

    return peopleIncreaseSalary;
}

function sumSalary(peopleSalary) {

    let increaseSalary = lowSalary(peopleSalary);
    let notIncreaseSalary = hightSalary(peopleSalary);

    sumSalary = increaseSalary + notIncreaseSalary;
    return sumSalary;

}



function showTableResult(peopleSalary) {
    let thName = "";
    let tableHeader = "";
    let tableRow = "";

    let sum = sumSalary(peopleSalary);

    let peopleLowSalary = showPeopleLowSalary(peopleSalary);
    console.log(peopleLowSalary);



    for (let i = 0; i < peopleLowSalary.length; i++) {
        thName = "";
        let tdValue = "";
        for (let key in peopleLowSalary[i]) {
            thName += "<th>" + key + "</th>";
        }

    }
    tableHeader = "<tr>" + thName + "</tr>";
    $('#peopleLowSalary').append($(tableHeader));


    for (let i = 0; i < peopleLowSalary.length; i++) {
        let tdValue = "";
        for (let key in peopleLowSalary[i]) {
            value = peopleLowSalary[i][key];
            tdValue += "<td>" + value + "</td>";

        }
        tableRow = "<tr>" + tdValue + "</tr>";
        $('#peopleLowSalary').append($(tableRow));
    }






    tableHeader2 = "<tr><th>sum</th><th>value</th></tr>"
    console.log("tableHeader2 is :" + tableHeader2);
    $('#sumSalary').append($(tableHeader2));

    let tdValue2 = '';
    let tableRow2 = '';

    console.log("sum value is:" + sum)
    tdValue2 += "<td>sum</td>"+"<td>" + sum + "</td>"
    tableRow2 = "<tr>" + tdValue2 + "</tr>";
    $('#sumSalary').append($(tableRow2));

}