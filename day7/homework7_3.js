
fetch('homework1-4.json').then(response => {
    return response.json();
}).then(jsonData => {

    new_jsonData = field_data(jsonData);
    new_jsonData = filter_gender(new_jsonData);
    new_jsonData = filter_friend(new_jsonData);
    new_jsonData = reduce_balance(new_jsonData);
    addTable(new_jsonData);


}).catch(error => {
    console.error('Error:', error);
});

function field_data(jsonData) {
    let new_jsonData = jsonData.map(jsonData => {
        let map_data = {};
        map_data.name = jsonData.name;
        map_data.gender = jsonData.gender;
        map_data.company = jsonData.company;
        map_data.email = jsonData.email;
        map_data.friends = jsonData.friends;
        map_data.balance = jsonData.balance;
        return map_data;
    });
    return new_jsonData;
}
function filter_gender(new_jsonData) {
    let people_gender = new_jsonData.filter(x => {
        return x.gender === "male";
    });
    return people_gender;
}
function filter_friend(new_jsonData) {
    let people_friend = new_jsonData.filter(x => {
        let array_friend = {};
        array_friend = x.friends;
        return Object.keys(array_friend).length > 1;
    });
    return people_friend;
}
function reduce_balance(new_jsonData) {
    let people_blance = new_jsonData.map(function (x) {
        let money;
        money = x.balance;
        money = money.replace('$', '');
        money = money.replace(',', '');
        money = parseFloat(money).toFixed(2);
        money = money / 10;
        money = money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        money = "$" + money;
        x.balance = money;
        return x;
    });
    return people_blance;
}
function addTable(new_jsonData) {
    for (let i = 0; i < new_jsonData.length; i++) {
        //debugger
        let array_new_jsonData = new_jsonData[i]
        let host_new_jsonData = "";
        if (new_jsonData[i] === new_jsonData[0]) {
            host_new_jsonData += "<tr>";
            for (let i in array_new_jsonData) {
                host_new_jsonData += "<td>" + i + "</td>";
            }
            host_new_jsonData += "</tr>";
            $('#myTable').append($(host_new_jsonData));
        }
        let table_new_jsonData = "";
        table_new_jsonData += "<tr>";
        for (let i in array_new_jsonData) {
            if (Array.isArray(array_new_jsonData[i]) === true) {
                friend_data = array_new_jsonData[i];
                table_new_jsonData += "<td><ol>";
                for (j = 0; j < friend_data.length; j++) {
                    friend_object_array = friend_data[j];
                    table_new_jsonData += "<li>"
                    for (k in friend_object_array) {
                        friend_table = JSON.parse(JSON.stringify(friend_object_array[k]));
                        table_new_jsonData += k + " " + friend_table + " ";
                    }
                    table_new_jsonData += "</li>";
                }
                table_new_jsonData += "</ol></td>";
            }
            else {
                table_new_jsonData += "<td>" + array_new_jsonData[i] + "</td>";
            }

        }
        table_new_jsonData += "</tr>";
        $('#myTable').append($(table_new_jsonData));
    }
}

