let fs = require('fs');
let allData = '';

function writeRobotFile() {
    console.log('allData');
    fs.readFile('head.txt', 'utf8', writeHead);
}

function writeHead(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    allData += data + '\n';
    console.log(allData);
    fs.readFile('body.txt', 'utf8', writeBody);
}

function writeBody(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    allData += data + '\n';
    console.log(allData);
    fs.readFile('leg.txt', 'utf8', writeLeg);
}

function writeLeg(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    allData += data + '\n';
    console.log(allData);
    fs.readFile('feet.txt', 'utf8', writeFeet);
}

function writeFeet(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    allData += data + '\n';
    console.log(allData);
    fs.writeFile('robot.txt', allData, 'utf8');
}

writeRobotFile();
